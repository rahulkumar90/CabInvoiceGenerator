package com.wipro.validations;

import com.wipro.bean.CabBean;
import java.util.regex.*;
import com.wipro.userexceptions.*;

public class TripValidator 
{
	static int[] arr = new int[2]; //Array to store the Receipt number and bill amount
	public static String printBillAmount(CabBean cabbean) throws NegativeKilometerException
	{
		int flag=0; //This variable will be incremented by 1 whenever a condition is validated
		
		//Validate the Booking ID
		if(Pattern.matches("AD\\d{5}",cabbean.getBookingID()))
		{
			flag++;
		}
		else
		{
			return "Invalid Booking ID";
		}
		
		//Validate the UserID 
		if(cabbean.getUserID()>=1001 && cabbean.getUserID()<=1500)
		{
			flag++;
		}
		else
		{
			return "Invalid user id";
		}
		
		//Validate the Cab Type
		if(cabbean.getCabType()=="BMU"||cabbean.getCabType()=="Tata Indica"||cabbean.getCabType()=="Tata Indigo"||cabbean.getCabType()=="Logan")
		{
			flag++;
		}
		else
		{
			return "Invalid Cab Type";
		}
		
		//Validate the Distance traveled
		int distance=Integer.parseInt(cabbean.getKmsUsed()); //Variable to store the kilometers used
		if(distance<0)
		{
			throw new NegativeKilometerException("Invalid Kilometer");
		}
		else
		{
			flag++;
		}
		
		//Generate the bill amount
		if(flag==4)
		{
			
			amountGenerator(distance,cabbean.getCabType());
		}
		cabbean.setTotalAmount(arr[1]);
		cabbean.setReceiptNo(arr[0]);
		return ("Total Amount : "+cabbean.getTotalAmount()+" , "+"Receipt ID : "+cabbean.getReceiptNo());
	}
	
	
	//Function to generate the bill and receipt Id
	public static int[] amountGenerator(int kmsUsed, String
			cabType)
	{
		int receiptNo = (int )(Math.random() * 9999 + 10000 ); //Calculate receipt ID
		
		arr[0]=receiptNo; //Store  receipt ID in array
		int totalAmount=0; //Store Total Bill Amount
		
		//Calculate the bill based on type of cab
		if(cabType=="Tata Indica")
		{
			totalAmount=(12*kmsUsed);
		}
		if(cabType=="Tata Indigo")
		{
			totalAmount=(10*kmsUsed);
		}
		if(cabType=="BMU")
		{
			totalAmount=(45*kmsUsed);
		}
		if(cabType=="Logan")
		{
			totalAmount=(31*kmsUsed);
		}
		arr[1]=totalAmount; //Store Total Amount in array
		return arr;
	}

}