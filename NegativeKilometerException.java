package com.wipro.userexceptions;

//Exception class for Negative Kilometer Distance

public class NegativeKilometerException extends Exception
{
	public NegativeKilometerException(String message)
	{
		super(message);
	}
}
