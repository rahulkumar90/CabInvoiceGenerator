package com.wipro.service;
import com.wipro.bean.CabBean;
import com.wipro.userexceptions.NegativeKilometerException;
import com.wipro.validations.TripValidator;
public class MainClass
{
public static void main(String[] args)
{
CabBean cabbean = new CabBean();
cabbean.setBookingID("AD12345");
cabbean.setCabType("BMU");
cabbean.setUserID(1003);
cabbean.setUsername("Hariprasath");

//Exception Handling for Negative distance
try
{
	cabbean.setKmsUsed("120");
	String result=TripValidator.printBillAmount(cabbean);
	System.out.println(result);
}
catch (NegativeKilometerException ex)
{
	System.err.print(ex);
}
}
}